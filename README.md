# media

Micropub Media Endpoint

Originaly written by aaron parecki, https://gist.github.com/aaronpk/4bee1753688ca9f3036d6f31377edf14


## Image Optimization 

If you want enable thumbnail optimization please see https://github.com/spatie/image-optimizer for requirements 
install the lib with :
```
composer require spatie/image-optimizer
```

find and uncomment this lines in endpoint.php :

```
require 'vendor/autoload.php';
use Spatie\ImageOptimizer\OptimizerChainFactory;
$optimizerChain = OptimizerChainFactory::create();
...
...
$optimizerChain->optimize($thumb_filename);
```
