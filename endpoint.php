<?php

require 'vendor/autoload.php';
use Spatie\ImageOptimizer\OptimizerChainFactory;
$optimizerChain = OptimizerChainFactory::create();

$token_endpoint = 'https://tokens.indieauth.com/token';
$base_url = 'https://jeer.fr/';
$media_url = 'https://media.jeer.fr/';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');

if(isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'text/plain') !== false) {
  $format = 'text';
} else {
  header('Content-Type: application/json');
  $format = 'json';
}

// Require access token

$headers = array(getallheaders());

  $token = $headers['0']['Authorization'];
  $ch = curl_init("https://tokens.indieauth.com/token");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, Array(
       "Content-Type: application/x-www-form-urlencoded"
      ,"Authorization: $token"
  ));
  $response = Array();
  parse_str(curl_exec($ch), $response);
  curl_close($ch);
  // Check for scope=create
  // Check for me=$configs->siteUrl
  $me = $response['me'];
  $iss = $response['issued_by'];
  $client = $response['client_id'];
  $scope = $response['scope'];
  $scopes = explode(' ', $scope); 
  if(empty($response)){
      header("HTTP/1.1 401 Unauthorized");
      echo 'The request lacks authentication credentials';
      die();
  } elseif ($me != "$base_url") {
      header("HTTP/1.1 401 Unauthorized");
      echo 'The request lacks valid authentication credentials';
      die();
  } elseif (!in_array('create', $scopes) && !in_array('media', $scopes)) {
      header("HTTP/1.1 403 Forbidden");
      echo json_encode([
        'error' => 'insufficient_scope',
        'error_description' => 'The access token provided does not have the necessary scope to upload files'
        ]);
      die();
  }

file_put_contents('/tmp/media_debug.log', '###############', FILE_APPEND);
file_put_contents('/tmp/media_debug.log', print_r($headers, true), FILE_APPEND);
file_put_contents('/tmp/media_debug.log', print_r($_FILES, true), FILE_APPEND);
file_put_contents('/tmp/media_debug.log', '###############', FILE_APPEND);

// Check for a file
if(!array_key_exists('file', $_FILES)) {
  header('HTTP/1.1 400 Bad Request');
  echo json_encode([
    'error' => 'invalid_request',
    'error_description' => 'The request must have a file upload named "file"'
  ]);
  die();
}

$file = $_FILES['file'];

$ext = mime_type_to_ext($file['type']);
if(!$ext) {
  $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
  if(!$ext)
    $ext = 'txt';
}

$filename = 'file-'.date('YmdHis').'-'.mt_rand(1000,9999).'.'.$ext;
copy($file['tmp_name'], $filename);

$url = $media_url . $filename;
header('HTTP/1.1 201 Created');
header('Location: '.$url);

if($format == 'text') {
  echo $url."\n";
} else {
  echo json_encode([
    'url' => $url
  ]);
}

switch ($ext) {
  case 'jpg';
  case 'png';
  case 'gif';
    if($ext == 'jpg') {
      $exif_json = json_encode(get_exif_data($filename), JSON_PRETTY_PRINT);
      $noext_filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
      $exif_filename = "exif_".$noext_filename.".json";
      file_put_contents($exif_filename, $exif_json);
    }
    $thumb_filename = 'thumb_'.$filename;
    $res_img = resize_image($filename, $filename, '600', '300');
    switch ($ext) {
      case 'jpg';
	imagejpeg($res_img, $thumb_filename);
      break;
      case 'png';
	imagepng($res_img, $thumb_filename);
      break;
      case 'gif';
	imagegif($res_img, $thumb_filename);
      break;
    }
    imagedestroy($res_img);
    $optimizerChain->optimize($thumb_filename);
}

function mime_type_to_ext($type) {
  $types = [
    'image/jpeg' => 'jpg',
    'image/pjpeg' => 'jpg',
    'image/gif' => 'gif',
    'image/png' => 'png',
    'image/x-png' => 'png',
    'image/svg' => 'svg',
    'audio/x-wav' => 'wav',
    'audio/wave' => 'wav',
    'audio/wav' => 'wav',
    'video/mpeg' => 'mpg',
    'video/quicktime' => 'mov',
    'video/mp4' => 'mp4',
    'audio/x-m4a' => 'm4a',
    'audio/mp3' => 'mp3',
    'audio/mpeg3' => 'mp3',
    'audio/mpeg' => 'mp3',
    'application/json' => 'json',
    'text/json' => 'json',
    'text/html' => 'html',
    'text/plain' => 'txt',
    'application/xml' => 'xml',
    'text/xml' => 'xml',
    'application/x-zip' => 'zip',
    'application/zip' => 'zip',
    'text/csv' => 'csv',
  ];
  if(array_key_exists($type, $types))
    return $types[$type];
  else {
    $fp = fopen('content-types.txt', 'a');
    fwrite($fp, "[".date("Y-m-d H:i:s")."]"."Unrecognized: $type\n");
    fclose($fp);
    return false;
  }
}
function get_exif_data($imagePath) {

    // Check if the variable is set and if the file itself exists before continuing
    if ((isset($imagePath)) and (file_exists($imagePath))) {
   
      // There are 2 arrays which contains the information we are after, so it's easier to state them both
      $exif_ifd0 = read_exif_data($imagePath ,'IFD0' ,0);      
      $exif_exif = read_exif_data($imagePath ,'EXIF' ,0);
     
      //error control
      $notFound = "Unavailable";
     
      // Make
      if (@array_key_exists('Make', $exif_ifd0)) {
        $camMake = $exif_ifd0['Make'];
      } else { $camMake = $notFound; }
     
      // Model
      if (@array_key_exists('Model', $exif_ifd0)) {
        $camModel = $exif_ifd0['Model'];
      } else { $camModel = $notFound; }
     
      // Exposure
      if (@array_key_exists('ExposureTime', $exif_ifd0)) {
        $camExposure = $exif_ifd0['ExposureTime'];
      } else { $camExposure = $notFound; }

      // Aperture
      if (@array_key_exists('ApertureFNumber', $exif_ifd0['COMPUTED'])) {
        $camAperture = $exif_ifd0['COMPUTED']['ApertureFNumber'];
      } else { $camAperture = $notFound; }
      
      // FocalLength
      if (@array_key_exists('FocalLength', $exif_ifd0)) {
        $camFocalLength = $exif_ifd0['FocalLength'];
      } else { $camFocalLength = $notFound; }

      // Flash
      if (@array_key_exists('Flash', $exif_ifd0)) {
        $camFlash = $exif_ifd0['Flash'];
      } else { $camFlash = $notFound; }
     
      // Date
      if (@array_key_exists('DateTime', $exif_ifd0)) {
        $camDate = $exif_ifd0['DateTime'];
      } else { $camDate = $notFound; }

      // FileName
      if (@array_key_exists('FileName', $exif_exif)) {
        $camFileName = $exif_exif['FileName'];
      } else { $camFileName = $notFound; }
     
      // ISO
      if (@array_key_exists('ISOSpeedRatings',$exif_exif)) {
        $camIso = $exif_exif['ISOSpeedRatings'];
      } else { $camIso = $notFound; }
     
      $return = array();
      $return['make'] = $camMake;
      $return['model'] = $camModel;
      $return['exposure'] = $camExposure;
      $return['aperture'] = $camAperture;
      $return['focallength'] = $camFocalLength;
      $return['date'] = $camDate;
      $return['iso'] = $camIso;
      $return['flash'] = $camFlash;
      $return['filename'] = $camFileName;
      return $return;
   
    } else {
      return false;
    }
}

function resize_image($filename, $tmpname, $xmax, $ymax)
{
	$ext = explode(".", $filename);
	$ext = $ext[count($ext)-1];

	if($ext == "jpg" || $ext == "jpeg" || $ext == "JPG")
		$im = imagecreatefromjpeg($tmpname);
	elseif($ext == "png")
		$im = imagecreatefrompng($tmpname);
	elseif($ext == "gif")
		$im = imagecreatefromgif($tmpname);

	$x = imagesx($im);
	$y = imagesy($im);

	if($x <= $xmax && $y <= $ymax)
		return $im;

	if($x >= $y) {
		$newx = $xmax;
		$newy = $newx * $y / $x;
	}
	else {
		$newy = $ymax;
		$newx = $x / $y * $newy;
	}

	$im2 = imagecreatetruecolor($newx, $newy);
	imagecopyresized($im2, $im, 0, 0, 0, 0, floor($newx), floor($newy), $x, $y);
	return $im2;
}
